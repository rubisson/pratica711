import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.if71c.pratica.Jogador;
import utfpr.ct.dainf.if71c.pratica.JogadorComparator;
import utfpr.ct.dainf.if71c.pratica.Time;

public class Pratica71 {

  public static void main(String[] args) {
    Time time = new Time();
    Scanner sc = new Scanner(System.in); 
    Jogador jog;
    // adicionando jogadores ao time
    int numeroJog,num=0,numero;
    String nome,pos;
    
    System.out.println("Digite o número de jogadores no time: ");
    numeroJog = Integer.parseInt(sc.nextLine());
    System.out.println("O valor de numeroJog é: " + numeroJog);
    
    while (num<numeroJog) {
   
        System.out.println("Digite a posição do jogador");
        pos = sc.nextLine();
        System.out.println("Digite o número do jogador");

        try{
            numero = Integer.parseInt(sc.nextLine());
            if (numero<0){
                throw new NumberFormatException("Digite apenas número maiores ou iguais a 0");
            }else{
                System.out.println("Digite o nome do jogador");
                nome = sc.nextLine();
                jog = new Jogador(numero, nome);
                time.addJogador(pos, jog);
                num++;
                System.out.println("valor do num: " + num);
                // ordem descendente de nome e ascendente de número
                JogadorComparator ordem = new JogadorComparator(true, true, false);
                List<Jogador> timeA = time.ordena(ordem);  

                timeA.forEach((Jogadores) -> {
                System.out.println(Jogadores.toString());
                  });
                do{
                    System.out.println("Digite a posição do jogador");
                    pos = sc.nextLine();
                    System.out.println("Digite o número do jogador");
                    numero = Integer.parseInt(sc.nextLine());
                    System.out.println("Digite o nome do jogador");
                    nome = sc.nextLine();
                    jog = new Jogador(numero, nome);
                    time.addJogador(pos, jog);
                }while(numero>0);
                sc.close();
                timeA.forEach((Jogadores) -> {
                    System.out.println(Jogadores.toString());
                });
            }
        }catch(NumberFormatException ex){
            System.out.println(ex.getMessage());
        }
    }
  }   
}