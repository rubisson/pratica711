package utfpr.ct.dainf.if71c.pratica;

public class Jogador implements Comparable<Jogador> {
  int numero;
  String nome;

  public Jogador(int numero, String nome) {
    this.numero = numero;
    this.nome = nome;
  }

  public int getNumero() {
    return numero;
  }

  public void setNumero(int numero) {
    this.numero = numero;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }
  
  @Override
  public String toString() {
    return numero + " - " + nome;
  }

  @Override
  public int compareTo(Jogador jogador) {
      if (numero<jogador.numero){
          return -1;
      }else{
          if (numero>jogador.numero){
              return +1;
          }else return 0;
      }
  }
}